$(document).ready(function() {

    /* ======= Scrollspy ======= */
   $('body').scrollspy({ target: '#page-nav-wrapper', offset: 100});
    
    /* ======= ScrollTo ======= */
    $('.scrollto').on('click', function(e){
        
        //store hash
        var target = this.hash;
                
        e.preventDefault();
        
		$('body').scrollTo(target, 800, {offset: -60, 'axis':'y'});
		
	});

  var my_nav = $('#page-nav-wrapper'); 
  // grab the initial top offset of the navigation 
  var topDistance = my_nav.offset().top;

  // our function that decides weather the navigation bar should have "fixed" css position or not.
  var sticky_navigation = function(){
    var scroll_top = $(window).scrollTop(); // our current vertical position from the top

    // if we've scrolled more than the navigation, change its position to stick to top, otherwise change it back to relative
    if (scroll_top > topDistance) { 
      my_nav.addClass( 'fixed' );
    } else {
      my_nav.removeClass( 'fixed' );
    }   
  };

  sticky_navigation();
	
	/* ======= Fixed page nav when scrolled ======= */    
  $(window).on('scroll resize load', function() {
    sticky_navigation();
  });
  
  /* ======= Chart ========= */
  $("#skills-section").appear(function(){
    $('.chart').easyPieChart({		
  		barColor:'#009688',//Pie chart colour
  		trackColor: '#e8e8e8',
  		scaleColor: false,
  		lineWidth : 5,
  		animate: 2000,
  		onStep: function(from, to, percent) {
  			$(this.el).find('span').text(Math.round(percent));
  		}
  	});
  },{accY: -200});
	

    
    /* ======= Isotope plugin ======= */
    /* Ref: http://isotope.metafizzy.co/ */
    // init Isotope    
    var $container = $('.isotope');
    
    $container.imagesLoaded(function () {
        $('.isotope').isotope({
            itemSelector: '.item'
        });
    });
    
    // filter items on click
    $('#filters').on( 'click', '.type', function() {
      var filterValue = $(this).attr('data-filter');
      $container.isotope({ filter: filterValue });
    });
    
    // change is-checked class on buttons
    $('.filters').each( function( i, typeGroup ) {
        var $typeGroup = $( typeGroup );
        $typeGroup.on( 'click', '.type', function() {
          $typeGroup.find('.active').removeClass('active');
          $( this ).addClass('active');
        });
    });
    

});