module.exports = function(grunt) {
  "use strict";
  require('jit-grunt')(grunt);

  // Project configuration
  grunt.initConfig({
    // Read package.json Metadata.
    pkg: grunt.file.readJSON('package.json'),
    banner: '/*******************************************************************************\n' +
    ' *              <%= pkg.name %> (version <%= pkg.version %>)\n' +
    ' *      Author: <%= pkg.author %>\n' +
    ' *  Created on: <%= grunt.template.today("mmmm dd,yyyy") %>\n' +
    ' *     Project: <%= pkg.name %>\n' +
    ' *   Copyright: Unlicensed.\n' +
    ' *******************************************************************************/\n',
    less: {
      dev: {
        options: {
          sourceMap: true,
        },
        files:{
          'dist/assets/css/bootstrap-customized.css':'less/bootstrap-customized.less',
          'dist/assets/css/bootstrap-rtl.css':'less/bootstrap-rtl.less',
          'dist/assets/css/<%= pkg.name %>.css':'less/<%= pkg.name %>.less',
          'dist/assets/css/<%= pkg.name %>-rtl.css':'less/<%= pkg.name %>-rtl.less'
        },
      },
      prod: {
        options: {
          compressed: true,
          cleancss: true,
          plugins: [
            new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]}),
          ],
        },
        files: {
          'dist/assets/css/bootstrap-customized.css':'less/bootstrap-customized.less',
          'dist/assets/css/bootstrap-rtl.css':'less/bootstrap-rtl.less',
          'dist/assets/css/<%= pkg.name %>.css':'less/<%= pkg.name %>.less',
          'dist/assets/css/<%= pkg.name %>-rtl.css':'less/<%= pkg.name %>-rtl.less'
        },
      },
      minify: {
        options: {
          cleancss: true,
          compressed: true,
        },
        files: {
          'dist/assets/css/bootstrap-customized.min.css':'less/bootstrap-customized.less',
          'dist/assets/css/bootstrap-rtl.min.css': 'less/bootstrap-rtl.less',
          'dist/assets/css/<%= pkg.name %>.min.css': 'less/<%= pkg.name %>.less',
          'dist/assets/css/<%= pkg.name %>-rtl.min.css': 'less/<%= pkg.name %>-rtl.less'
        }
      }
    },

    usebanner: {
      options: {
        position: 'top',
        banner: '<%= banner %>',
        linebreak: true

      },
      files: {
        src: ['dist/assets/css/<%= pkg.name %>.css', 'dist/assets/css/<%= pkg.name %>.min.css']
      }
    },

    connect: {
      less:{
        options: {
          port: 3000,
          hostname: 'localhost',
          base: '.',
          livereload: true,
          open: {
            target: 'http://localhost:3000/dist/index.html', // target url to open
            callback: function() {

            } // called when the app has opened
          }
        }
      }
    },

    watch: {
      dev: {
        files: ['less/**/*.less','pages/**/*.html'],
        tasks: ['less:dev','less:minify','copy:dist'],
        options: {
          livereload: true,
        },
      },
      prod: {
        files: ['less/**/*.less','pages/**/*.html'],
        tasks: ['less:prod','less:minify','copy:dist'],
        options: {
          livereload: true,
        },
      }
    },

    copy: {
      main: {
        files: [
          {expand: true, cwd: 'bower_components/bootstrap/less/', src: ['**'], dest: 'less/bootstrap/less/'},
          {expand: true, cwd: 'bower_components/bootstrap-rtl/less/', src: ['**'], dest: 'less/bootstrap-rtl/'},
          {expand: true, cwd: 'bower_components/jquery/dist/', src: ['**'], dest: 'assets/js/'}
        ]
      },
      dist: {
        files: [
          {expand: true, cwd: 'dist/assets/css/', src: ['**'], dest: 'assets/css/'},
          {expand: true, cwd: 'assets/', src: ['**'], dest: 'dist/assets'},
          {expand: true, cwd: 'pages/', src: ['**'], dest: 'dist/'}
        ]
      }
    },

    exec: {
      init: {
        command: 'npm update'
      },
      deps: {
        command: 'npm install; bower install',
        stdout: false
      }
    }


  });


  require('load-grunt-tasks')(grunt, {
    scope: 'devDependencies'
  });

  // Default Task

  grunt.registerTask('default', ['exec:init','copy:main','less:prod','less:minify','copy:dist']);
  grunt.registerTask('serve', ['less:prod','less:minify','usebanner','copy:dist','connect','watch:prod']);
  grunt.registerTask('init', ['exec:deps']);
  grunt.registerTask('build', ['less:prod','less:minify','usebanner','copy:dist']);
  grunt.registerTask('dev', ['copy:main','less:dev','less:minify','copy:dist','connect','watch:dev']);
};
